<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $products = [];

        $raw = json_decode(
            file_get_contents(resource_path('products.json')),
            true
        );

        foreach ($raw as $product) {
            $products[] = [
                'name' => $product['name'],
                'price' => $product['price'],
                'bedrooms' => $product['bedrooms'],
                'bathrooms' => $product['bathrooms'],
                'storeys' => $product['storeys'],
                'garages' => $product['garages']
            ];
        }

        DB::table('products')->insert($products);
    }
}
