<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Searchable;

	/**
	 * Get the indexable data array for the model.
	 *
	 * @return array
	 */
	public function toSearchableArray()
	{
		  $array = $this->toArray();
		  return array(
		  	'id' => $array['id'],
		  	'name' => $array['name'], 
		  	'price' => $array['price'], 
		  	'bedrooms' => $array['bedrooms'], 
		  	'bathrooms' => $array['bathrooms'], 
		  	'storeys' => $array['storeys'], 
		  	'garages' => $array['garages']
		  );
	}

}